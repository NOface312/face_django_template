# Face_Django_Template

## Requirements
-  [ ] OS - Linux (Ubuntu or other)
-  [ ] Docker (CLI)
-  [ ] Docker Compose (CLI)

## Getting started
### Before Development(Git)
At the beginning, clone project:
```
git clone rep_url
```
Due to the DevOps methodology, you as a developer, CAN'T PUSH DIRECTLY PUSH TO THE MAIN BRANCH of the project. 
But how then to develop the project?
First you need create your own branch, were you can write your changes:
```
git branch your_branch_name
```
When checkout to this branch:
```
git checkout your_branch_name
```
When you and your features and want to push changes to your branch:
```
git add .
git commit -m "Realse my feature"
git push origin your_name_branch
```
Okey, if pipelines jobs ends successfully, you can merge your changes to main branch.
In gitlab project page. Choose your branch and push to the button create merge request.
Wait when project manager approve your changes and add them.
When branhces merged your branch will be deleted on global repository, but he stay on your local repository.
Delete your branch in local repository, pull changes and create new branch:
```
git branch -d your_old_name
git pull
git branch your_branch_name
```
### Debug mode
For start project:
(Docker-compose build images and start containers)
```
docker-compose -f docker-compose.yaml up
#OR
docker-compose -f docker-compose.yaml up -d
#OR(Recomended)
docker-compose -f docker-compose.yaml up -d --build
```
Key "-d" - running containers in the background mode(daemon mod), without console output
Key "--build" - Build images before starting containers.

For stop project:
```
docker-compose -f docker-compose.yaml down
#OR
docker-compose -f docker-compose.yaml down -v
```
Key "-v" - for remove mount volumes

### Production debug mode
IMPORTANT: Unlike debug mode in this case project will be launched with nginx and gunicorn 
For start project:
(Docker-compose build images and start containers)
```
docker-compose -f docker-compose.debug_server.yaml up
#OR
docker-compose -f docker-compose.debug_server.yaml up -d
#OR(Recomended)
docker-compose -f docker-compose.debug_server.yaml up -d --build
```
Key "-d" - running containers in the background mode(daemon mod), without console output
Key "--build" - Build images before starting containers.

For stop project:
```
docker-compose -f docker-compose.debug_server.yaml down
#OR
docker-compose -f docker-compose.debug_server.yamldown -v
```
Key "-v" - for remove mount volumes

### For resovle problems with docker and clear docker
If you have some issues or bugs with Docker. Launch this command for hard reset project:
```
#Stop containers and remove volumes
docker-compose -f your_config.yaml down -v
#Remove containers
docker rm $(docker ps -aq)
#Remove images
docker rmi $(docker images -aq)
```
And now I hope your problems will be solved

## Description(How it's works)
I will not explain all the processes in my project and each file. Instead of this, I'll will explain the contents of the manifest files(yaml) and walk through each of them.
But first, let's go through the structure of the project.

### Project structure
-  backend - contains dockerfile and .env files for django image
    - app - python code 
    - static - static files for images building
-  data - env files for database image
-  nginx - dockerfile and nginx config

### Manifest files
-  docker-compose.cicd - Used during the passage of pipelines in GitLab CI CD
-  docker-compose.prod - Used for deploying
-  docker-compose.debug_server - Very similar to production file, but used for debugging nginx server
-  docker-compose - for development, used for debug django project.
-  gitlab-ci.yaml - manifest file for run pipeline in gitlab. This is the CI CD
