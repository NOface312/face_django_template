#!/bin/bash

#DRAW FUNCTIONS
#MAIN MENU
draw_main_menu() {
    echo "-----------------"
    echo "|   MAIN MENU   |"
    echo "-----------------"
    echo "|\"debug\"              - debug mode"
    echo "|\"prod\"               - prod mode"
    echo "|\"stop\"               - stop and remove containers"
    echo "|\"remove_containres\"  - remove all containers"
    echo "|\"remove_images\"      - remove all images"
    echo "|\"exit\"               - exit"
}

draw_debug_menu() {
    echo "-----------------"
    echo "|   DEBUG MENU  |"
    echo "-----------------"
    echo "|\"build\"              - build project"
    echo "|\"debug\"              - launch project"
    echo "|\"stop\"               - stop and remove containers"
    echo "|\"remove_containres\"  - remove all containers"
    echo "|\"remove_images\"      - remove all images"
    echo "|\"back\"               - back to previous menu"
    echo "|\"exit\"               - exit"
}

draw_prod_menu() {
    echo "-----------------"
    echo "|   PROD MENU   |"
    echo "-----------------"
    echo "|\"build\"              - build project"
    echo "|\"debug\"              - launch project"
    echo "|\"stop\"               - stop and remove containers"
    echo "|\"remove_containres\"  - remove all containers"
    echo "|\"remove_images\"      - remove all images"
    echo "|\"back\"               - back to previous menu"
    echo "|\"exit\"               - exit"
}

#SUB MENUS
debug_menu() {
    while :
    do
        sleep 1
        clear
        draw_debug_menu
        read -p "Enter choise: " CHOISE
        case $CHOISE in
        build)
            echo "BUILDING PROJECT"
            docker-compose -f docker-compose.yaml build
            ;;
        debug)
            echo "RUN RPOJECT..."
            docker-compose -f docker-compose.yaml up -d
            ;;
        stop)
            echo "STOP CONTAINERS..."
            docker-compose down -v
            echo "DONE"
            ;;
        remove_containres)
            echo "REMOVE CONTAINERS..."
            docker rm $(docker ps -aq)
            echo "DONE"
            ;;
        remove_images)
            echo "REMOVE IMAGES..."
            docker rmi $(docker images -aq)
            echo "DONE"
            ;;
        back)
            break
            ;;
        exit)
            echo "Exit script"
            echo "Don't forget stop containers"
            exit
            ;;
        *)
            echo "Input Error"
            ;;
        esac
    done
}

prod_mode() {
    while :
    do
        sleep 1
        clear
        draw_prod_menu
        read -p "Enter choise: " CHOISE
        case $CHOISE in
        build)
            echo "BUILDING PROJECT..."
            docker-compose -f docker-compose.debug_server.yaml build
            echo "DONE"
            ;;
        debug)
            echo "RUN PROJECT..."
            docker-compose -f docker-compose.debug_server.yaml up -d
            echo "DONE"
            ;;
        stop)
            echo "STOP CONTAINERS..."
            docker-compose down -v
            echo "DONE"
            ;;
        remove_containres)
            echo "REMOVE CONTAINERS..."
            docker rm $(docker ps -aq)
            echo "DONE"
            ;;
        remove_images)
            echo "REMOVE IMAGES..."
            docker rmi $(docker images -aq)
            echo "DONE"
            ;;
        back)
            break
            ;;
        exit)
            echo "Exit script"
            echo "Don't forget stop containers"
            exit
            ;;
        *)
            echo "Input Error"
            ;;
        esac
    done
}

# MAIN LOOP
while :
do
    sleep 1
    clear
    draw_main_menu
    read -p "Enter choise: " CHOISE
    case $CHOISE in
    debug)
        debug_menu
        ;;
    prod)
        prod_menu
        ;;
    stop)
        echo "STOP CONTAINERS..."
        docker-compose down -v
        echo "DONE"
        ;;
    remove_containres)
        echo "REMOVE CONTAINERS..."
        docker rm $(docker ps -aq)
        echo "DONE"
        ;;
    remove_images)
        echo "REMOVE IMAGES..."
        docker rmi $(docker images -aq)
        echo "DONE"
        ;;
    exit)
        echo "Exit script"
        echo "Don't forget stop containers"
        exit
        ;;
    *)
        echo "Input Error"
        ;;
    esac
done