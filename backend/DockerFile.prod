###########
# BUILDER #
###########

# pull official base image
FROM python:alpine3.14 as builder

# author label
LABEL author = "nofacexxx4@gmail.com"

# set enviroment variables
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

#install postgresql and python dependence
RUN apk update \
    && apk add postgresql-client\
    && rm -rf /var/lib/apt/lists/* \
    && apk add postgresql-dev gcc python3-dev musl-dev 

# upgrade pip tools, install flake8
RUN pip install --upgrade pip setuptools wheel \
    && pip install flake8==4.0.1

WORKDIR /usr/src/app
COPY ./app .

# lint
RUN flake8 --ignore=E501,F401 .

WORKDIR /usr/src/static
COPY ./static .

# install dependencies
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r /usr/src/static/requirements.txt

#########
# FINAL #
#########

# pull official base image
FROM python:alpine3.14

# create directory for the app user
RUN mkdir -p /home/app

# create the app user
RUN addgroup -S app && adduser -S app -G app

# create the appropriate directories
ENV HOME=/home/app
ENV APP_HOME=/home/app/backend
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# install dependencies
RUN apk update && apk add libpq netcat-openbsd
COPY --from=builder /usr/src/app/wheels /wheels
COPY --from=builder /usr/src/static/requirements.txt .
RUN pip install --no-cache /wheels/*

# copy entrypoint.prod.sh
COPY ./static/entrypoint.prod.sh .
RUN sed -i 's/\r$//g'  $APP_HOME/entrypoint.prod.sh
RUN chmod +x  $APP_HOME/entrypoint.prod.sh

# copy project
COPY ./app $APP_HOME

# chown all the files to the app user
RUN chown -R app:app $APP_HOME

# change to the app user
USER app

# run entrypoint.prod.sh
ENTRYPOINT ["/home/app/backend/entrypoint.prod.sh"]